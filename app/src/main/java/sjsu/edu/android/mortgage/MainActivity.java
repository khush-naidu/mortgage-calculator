package sjsu.edu.android.mortgage;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.CheckBox;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {


    private EditText borrowedamount;
    private SeekBar seekbar;
    private TextView seekprogress;
    private TextView monthlypayment;
    float prog;
    float amount;
    float payment = 0;
    float tax = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        borrowedamount = (EditText) findViewById(R.id.Amount);
        monthlypayment = (TextView) findViewById(R.id.payment);
        seekbar = (SeekBar) findViewById(R.id.seekBar);
        seekprogress = (TextView) findViewById(R.id.seekprogress);

        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                seekprogress.setText(String.valueOf(progress));
                prog = progress;
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

        });
    }

    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.CalculateButton:
                RadioButton term1 = (RadioButton) findViewById(R.id.fifteen);
                RadioButton term2 = (RadioButton) findViewById(R.id.twenty);
                RadioButton term3 = (RadioButton) findViewById(R.id.thirty);
                CheckBox taxandins = (CheckBox) findViewById(R.id.TaxCheck);


                if (borrowedamount.getText().length() <= 0) {
                    Toast.makeText(this, "Please enter a valid number",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                amount = Float.parseFloat(borrowedamount.getText().toString());
                if (amount < 0) {
                Toast.makeText(this, "Please enter a valid number",
                        Toast.LENGTH_LONG).show();
                    return;
                }
                System.out.println("radio1" + term1.isChecked());
                System.out.println("radio2" + term2.isChecked());
                System.out.println("radio3" + term3.isChecked());
                if (term1.isChecked()) {
                    Calculator.setYears(15);
                    term1.setChecked(true);
                    term2.setChecked(false);
                    term3.setChecked(false);
                } else if (term2.isChecked()) {
                    Calculator.setYears(20);
                    term1.setChecked(false);
                    term2.setChecked(true);
                    term3.setChecked(false);
                } else if (term3.isChecked()) {
                    Calculator.setYears(30);
                    term1.setChecked(false);
                    term2.setChecked(false);
                    term3.setChecked(true);
                }
                if (taxandins.isChecked()) {
                    taxandins.setChecked(true);
                    tax = amount/1000;
                    payment = Calculator.CalculateMortgage(Calculator.getYears(),
                            amount,
                            seekbar.getProgress(), tax);
                } else {
                    taxandins.setChecked(false);
                    Calculator.setInterest(Float.parseFloat(seekprogress.getText().toString()));
                    tax = amount/1000;
                    payment = Calculator.CalculateMortgage(Calculator.getYears(),
                            amount,
                            seekbar.getProgress(), 0);
                }
                
                monthlypayment.setText(String.valueOf(payment));
                break;
        }
    }

}