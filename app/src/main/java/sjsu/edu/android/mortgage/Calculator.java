package sjsu.edu.android.mortgage;
import java.lang.Math.*;

public class Calculator {

    static float interest;
    static float payment;
    static float years;
    public static void setInterest(float annualIR)
    {
        interest = annualIR;
    }
    public static float getInterest()
    {
        return interest;
    }

    public static void setYears(float term)
    {
        years = term;
    }
    public static float getYears()
    {
        return years;
    }
    public static float CalculateMortgage(float year, float amount, float interest1, float tax)
    {
        float months;
        interest = interest1;
        year = getYears();
        months = year * 12;
        if(interest > 0)
        {
            payment = (float) ((amount * ((interest/1200)/(1 - (Math.pow((1 + interest/1200), (-1 * months)))))) + tax);
        }
        else
        {
            payment = (amount/months) + tax;
        }
        return payment;
    }
    public static float getMonthlyPayment()
    {
        return payment;
    }
}
